*** Settings ***
Library    SeleniumLibrary
Resource    ../Keyword/ToDoListKeyword.resource

*** Test Cases ***
TC0001
    [Documentation]    To verify that the To-Do-List website is displayed correctly when click on the To Do List website.
    Open To Do List page via Chrome
    Verifly Title Of To Do List Page
    Verifly ADD ITEM Tab
    Verifly TO-DO TASKS Tab
    Verifly COMPLETED Tab
    Verifly Input Task InputText    text=${EMPTY}
    Verifly Add Button
    Close Browser

TC002
    [Documentation]    To verify that the To-Do-List website is working correctly, when click on TO-DO TASKS tab.
    Open To Do List page via Chrome
    Click TO-DO TASKS Tab
    Close Browser

TC003
    [Documentation]    To verify that the To-Do-List website is working correctly, when click on COMPLETE tab.
    Open To Do List page via Chrome
    Click COMPLETED Tab
    Close Browser

TC004
    [Documentation]    To verify that the To-Do-List website is working correctly, when click on any tab and click ADD ITEM tab.
    Open To Do List page via Chrome
    Click TO-DO TASKS Tab
    Click COMPLETED Tab
    Click ADD ITEM Tab
    Close Browser

TC005
    [Documentation]    To verify that the To-Do-List website is working correctly, when add list on ADD ITEM tab.
    ${taskText}    Set Variable    Reading a book
    Open To Do List page via Chrome
    Click ADD ITEM Tab
    Input Task    text=${taskText}
    Click Add Button
    Verifly Input Task InputText    text=${EMPTY}
    Click TO-DO TASKS Tab
    Verifly Item On TO-DO TASKS Tab    position=1    text=${taskText}
    Close Browser

TC006
    [Documentation]    To verify that the To-Do-List website is working correctly, when delete list on TO-DO TASKS tab.
    ${taskTextA}    Set Variable    Reading a book A
    ${taskTextB}    Set Variable    Reading a book B
    Open To Do List page via Chrome
    Click ADD ITEM Tab
    Input Task    text=${taskTextA}
    Click Add Button
    Verifly Input Task InputText    text=${EMPTY}
    Input Task    text=${taskTextB}
    Click Add Button
    Click TO-DO TASKS Tab
    Verifly Item On TO-DO TASKS Tab    position=1    text=${taskTextA}
    Verifly Item On TO-DO TASKS Tab    position=2    text=${taskTextB}
    Click Delete Button On TO-DO TASKS Tab    position=1
    Verifly Item On TO-DO TASKS Tab    position=1    text=${taskTextB}
    Close Browser

TC007
    [Documentation]    To verify that the To-Do-List website is working correctly, when delete list on COMPLETE  tab.
    ${taskTextA}    Set Variable    Reading a book A
    ${taskTextB}    Set Variable    Reading a book B
    Open To Do List page via Chrome
    Click ADD ITEM Tab
    Input Task    text=${taskTextA}
    Click Add Button
    Input Task    text=${taskTextB}
    Click Add Button
    Click TO-DO TASKS Tab
    Verifly Item On TO-DO TASKS Tab    position=1    text=${taskTextA}
    Verifly Item On TO-DO TASKS Tab    position=2    text=${taskTextB}
    Click Checkbox On TO-DO TASKS Tab    position=1
    Verifly Item On TO-DO TASKS Tab    position=1    text=${taskTextB}
    Click Checkbox On TO-DO TASKS Tab    position=1
    Click COMPLETED Tab
    Verifly Item On COMPLETED Tab    position=1    text=${taskTextA}
    Verifly Item On COMPLETED Tab    position=2    text=${taskTextB}
    Click Delete Button On COMPLETED Tab    position=1
    Verifly Item On COMPLETED Tab    position=1    text=${taskTextB}
    Close Browser

TC008
    [Documentation]    To verify that the To-Do-List website is working correctly, when click checkbox on list at TO-DO TASKS tab.
    ${taskText}    Set Variable    Reading a book
    Open To Do List page via Chrome
    Click ADD ITEM Tab
    Input Task    text=${taskText}
    Click Add Button
    Click TO-DO TASKS Tab
    Verifly Item On TO-DO TASKS Tab    position=1    text=${taskText}
    Click Checkbox On TO-DO TASKS Tab    position=1
    Click COMPLETED Tab
    Verifly Item On COMPLETED Tab    position=1    text=${taskText}
    Close Browser